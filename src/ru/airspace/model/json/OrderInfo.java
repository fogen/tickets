package ru.airspace.model.json;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * Created by Andrey Semenyuk on 2017.
 */
public class OrderInfo {

    private boolean canAddNewOrderParts;
    private List<OrderPart> orderParts;
    private List<OrderSegment> segments;
    private String passengersType;

    public boolean isCanAddNewOrderParts() {
        return canAddNewOrderParts;
    }

    public void setCanAddNewOrderParts(boolean canAddNewOrderParts) {
        this.canAddNewOrderParts = canAddNewOrderParts;
    }

    public List<OrderPart> getOrderParts() {
        return orderParts;
    }

    public void setOrderParts(List<OrderPart> orderParts) {
        this.orderParts = orderParts;
    }

    public List<OrderSegment> getSegments() {
        return segments;
    }

    public void setSegments(List<OrderSegment> segments) {
        this.segments = segments;
    }

    public String getPassengersType() {
        return passengersType;
    }

    public void setPassengersType(String passengersType) {
        this.passengersType = passengersType;
    }

    public String getSeatNumber() {

        if (orderParts != null &&
            orderParts.get(0).getPassengers() != null &&
            orderParts.get(0).getPassengers().get(0).getSegments().get(0).getAlreadyAssignedSeatInAstra() != null) {

            String number = orderParts.get(0).getPassengers().get(0).getSegments().get(0).getAlreadyAssignedSeatInAstra().getNumber();

            if (number != null) {
                return number;
            }
        }

        return "";
    }

    public String getDocumentsAndFFP() {

        JSONArray documentsAndFFP = new JSONArray();
        JSONObject resultContent = new JSONObject();
        Passenger passenger = getOrderParts().get(0).getPassengers().get(0);
        resultContent.put("astraVirtualId", passenger.getAstraVirtualId());

        JSONObject targetSegment = new JSONObject();

        passenger.getSegments().forEach((PassengerSegment segment) -> {
            targetSegment.put("document", getNameValueObj(segment.getDocument().getFields()));

            Visa visa = segment.getVisa();

            if (visa != null && visa.getRequired()) {
                targetSegment.put("visa", getNameValueObj(segment.getVisa().getFields()));
            }

            if (segment.getFfpCardNumber() != null) {
                targetSegment.put("ffpCardNumber", segment.getFfpCardNumber());
            }
        });

        JSONArray segments = new JSONArray();
        segments.add(targetSegment);

        resultContent.put("segments", segments);
        documentsAndFFP.add(resultContent);

        return documentsAndFFP.toString();
    }

    public String getPassengerData() {

        JSONObject passengerData = new JSONObject();
        Passenger passenger = getOrderParts().get(0).getPassengers().get(0);
        PassengerSegment segment = passenger.getSegments().get(0);

        boolean visaCheckboxNotRequired = segment.getVisa() != null && segment.getVisa().getRequired();
        passengerData.put("document", new JSONObject());
        passengerData.put("visaCheckboxNotRequired", visaCheckboxNotRequired);
        passengerData.put("visa", visaCheckboxNotRequired ? segment.getVisa() : new JSONObject());

        return passengerData.toString();
    }

    private Object getNameValueObj(List<Field> fields) {

        JSONObject result = new JSONObject();

        fields.forEach(field -> result.put(field.getName(), field.getValue()));

        return result;
    }
}

class OrderSegment {

    private Stages stages;
    private String departureCity;
    private String arrivalCity;
    private String arrivalAirport;
    private String departureAirport;
    private String arrivalDateTime;
    private String arrivalDate;
    private String arrivalTime;
    private String departureDateTime;
    private String departureDate;
    private String departureTime;
    private String airline;
    private String airlineCode;
    private String flightNumber;
    private String aircraft;
    private String aircraftCode;
    private String status;
    private String flightSubclass;
    private String flightClass;
    private String selectSeatsMode;

    public Stages getStages() {
        return this.stages;
    }

    public void setStages(Stages stages) {
        this.stages = stages;
    }

    public String getDepartureCity() {
        return this.departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getArrivalCity() {
        return this.arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getArrivalAirport() {
        return this.arrivalAirport;
    }

    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    public String getDepartureAirport() {
        return this.departureAirport;
    }

    public void setDepartureAirport(String departureAirport) {
        this.departureAirport = departureAirport;
    }

    public String getArrivalDateTime() {
        return this.arrivalDateTime;
    }

    public void setArrivalDateTime(String arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public String getArrivalDate() {
        return this.arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getArrivalTime() {
        return this.arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDepartureDateTime() {
        return this.departureDateTime;
    }

    public void setDepartureDateTime(String departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public String getDepartureDate() {
        return this.departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureTime() {
        return this.departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getAirline() {
        return this.airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getAirlineCode() {
        return this.airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getFlightNumber() {
        return this.flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public String getAircraft() {
        return this.aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    public String getAircraftCode() {
        return this.aircraftCode;
    }

    public void setAircraftCode(String aircraftCode) {
        this.aircraftCode = aircraftCode;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFlightSubclass() {
        return this.flightSubclass;
    }

    public void setFlightSubclass(String flightSubclass) {
        this.flightSubclass = flightSubclass;
    }

    public String getFlightClass() {
        return this.flightClass;
    }

    public void setFlightClass(String flightClass) {
        this.flightClass = flightClass;
    }

    public String getSelectSeatsMode() {
        return this.selectSeatsMode;
    }

    public void setSelectSeatsMode(String selectSeatsMode) {
        this.selectSeatsMode = selectSeatsMode;
    }
}

class Stages {

    private String webCheckInOpen;
    private String webCheckInClose;
    private String airportCheckInOpen;
    private String airportCheckInClose;
    private String boardingOpen;
    private String boardingClose;
    private String cancelClose;

    public String getWebCheckInOpen() {
        return this.webCheckInOpen;
    }

    public void setWebCheckInOpen(String webCheckInOpen) {
        this.webCheckInOpen = webCheckInOpen;
    }

    public String getWebCheckInClose() {
        return this.webCheckInClose;
    }

    public void setWebCheckInClose(String webCheckInClose) {
        this.webCheckInClose = webCheckInClose;
    }

    public String getAirportCheckInOpen() {
        return this.airportCheckInOpen;
    }

    public void setAirportCheckInOpen(String airportCheckInOpen) {
        this.airportCheckInOpen = airportCheckInOpen;
    }

    public String getAirportCheckInClose() {
        return this.airportCheckInClose;
    }

    public void setAirportCheckInClose(String airportCheckInClose) {
        this.airportCheckInClose = airportCheckInClose;
    }

    public String getBoardingOpen() {
        return this.boardingOpen;
    }

    public void setBoardingOpen(String boardingOpen) {
        this.boardingOpen = boardingOpen;
    }

    public String getBoardingClose() {
        return this.boardingClose;
    }

    public void setBoardingClose(String boardingClose) {
        this.boardingClose = boardingClose;
    }

    public String getCancelClose() {
        return this.cancelClose;
    }

    public void setCancelClose(String cancelClose) {
        this.cancelClose = cancelClose;
    }
}

class OrderPart {

    private String numberFromInput;
    private String lastNameFromInput;
    private String flightFromInput;
    private String departureDateFromInput;
    private List<Passenger> passengers;

    public String getNumberFromInput() {
        return this.numberFromInput;
    }

    public void setNumberFromInput(String numberFromInput) {
        this.numberFromInput = numberFromInput;
    }

    public String getLastNameFromInput() {
        return this.lastNameFromInput;
    }

    public void setLastNameFromInput(String lastNameFromInput) {
        this.lastNameFromInput = lastNameFromInput;
    }

    public String getFlightFromInput() {
        return this.flightFromInput;
    }

    public void setFlightFromInput(String flightFromInput) {
        this.flightFromInput = flightFromInput;
    }

    public String getDepartureDateFromInput() {
        return this.departureDateFromInput;
    }

    public void setDepartureDateFromInput(String departureDateFromInput) {
        this.departureDateFromInput = departureDateFromInput;
    }

    public List<Passenger> getPassengers() {
        return this.passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

}

class Passenger {

    private boolean available;
    private String astraVirtualId;
    private String ticketNumbers;
    private String name;
    private String lastName;
    private String birthDate;
    private boolean lead;
    private boolean selected;
    private String child;
    private boolean isAvailable;
    private String unavailabilityReason;
    private List<PassengerSegment> segments;
    private List<String> possibleActions;

    public boolean getAvailable() {
        return this.available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getAstraVirtualId() {
        return this.astraVirtualId;
    }

    public void setAstraVirtualId(String astraVirtualId) {
        this.astraVirtualId = astraVirtualId;
    }

    public String getTicketNumbers() {
        return this.ticketNumbers;
    }

    public void setTicketNumbers(String ticketNumbers) {
        this.ticketNumbers = ticketNumbers;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return this.birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public boolean getLead() {
        return this.lead;
    }

    public void setLead(boolean lead) {
        this.lead = lead;
    }

    public boolean getSelected() {
        return this.selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getChild() {
        return this.child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public boolean getIsAvailable() {
        return this.isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public String getUnavailabilityReason() {
        return this.unavailabilityReason;
    }

    public void setUnavailabilityReason(String unavailabilityReason) {
        this.unavailabilityReason = unavailabilityReason;
    }

    public List<PassengerSegment> getSegments() {
        return this.segments;
    }

    public void setSegments(List<PassengerSegment> segments) {
        this.segments = segments;
    }

    public List<String> getPossibleActions() {
        return this.possibleActions;
    }

    public void setPossibleActions(List<String> possibleActions) {
        this.possibleActions = possibleActions;
    }
}

class PassengerSegment {

    private Visa visa;
    private Document document;
    private AlreadyAssignedSeatInAstra alreadyAssignedSeatInAstra;
    private ChosenSeatThroughGui chosenSeatThroughGui;
    private String ffpCardNumber;
    private boolean assignedAnotherSeat;
    private List<Action> possibleActions;
    private BagNorm bagNorm;

    public Visa getVisa() {
        return this.visa;
    }

    public void setVisa(Visa visa) {
        this.visa = visa;
    }

    public Document getDocument() {
        return this.document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public AlreadyAssignedSeatInAstra getAlreadyAssignedSeatInAstra() {
        return this.alreadyAssignedSeatInAstra;
    }

    public void setAlreadyAssignedSeatInAstra(AlreadyAssignedSeatInAstra alreadyAssignedSeatInAstra) {
        this.alreadyAssignedSeatInAstra = alreadyAssignedSeatInAstra;
    }

    public ChosenSeatThroughGui getChosenSeatThroughGui() {
        return this.chosenSeatThroughGui;
    }

    public void setChosenSeatThroughGui(ChosenSeatThroughGui chosenSeatThroughGui) {
        this.chosenSeatThroughGui = chosenSeatThroughGui;
    }

    public String getFfpCardNumber() {
        return this.ffpCardNumber;
    }

    public void setFfpCardNumber(String ffpCardNumber) {
        this.ffpCardNumber = ffpCardNumber;
    }

    public boolean getAssignedAnotherSeat() {
        return this.assignedAnotherSeat;
    }

    public void setAssignedAnotherSeat(boolean assignedAnotherSeat) {
        this.assignedAnotherSeat = assignedAnotherSeat;
    }

    public List<Action> getPossibleActions() {
        return this.possibleActions;
    }

    public void setPossibleActions(List<Action> possibleActions) {
        this.possibleActions = possibleActions;
    }

    public BagNorm getBagNorm() {
        return this.bagNorm;
    }

    public void setBagNorm(BagNorm bagNorm) {
        this.bagNorm = bagNorm;
    }
}

class BagNorm {

    private String message;
    private boolean available;

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean getAvailable() {
        return this.available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}

class ChosenSeatThroughGui {

    private String number;
    private String currency;
    private String cost;
    private String paid;

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCost() {
        return this.cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getPaid() {
        return this.paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }
}

class AlreadyAssignedSeatInAstra {

    private String number;
    private String currency;
    private String cost;
    private String paid;

    public String getNumber() {
        return this.number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCurrency() {
        return this.currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCost() {
        return this.cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getPaid() {
        return this.paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }
}

class Document {

    private boolean international;
    private boolean isInternational;
    private List<Field> fields;

    public boolean getInternational() {
        return this.international;
    }

    public void setInternational(boolean international) {
        this.international = international;
    }

    public boolean getIsInternational() {
        return this.isInternational;
    }

    public void setIsInternational(boolean isInternational) {
        this.isInternational = isInternational;
    }

    public List<Field> getFields() {
        return this.fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }
}

class Visa {

    private boolean international;
    private boolean allFieldsEmpty;
    private boolean required;
    private boolean isInternational;
    private List<Field> fields;

    public boolean getInternational() {
        return this.international;
    }

    public void setInternational(boolean international) {
        this.international = international;
    }

    public boolean getAllFieldsEmpty() {
        return this.allFieldsEmpty;
    }

    public void setAllFieldsEmpty(boolean allFieldsEmpty) {
        this.allFieldsEmpty = allFieldsEmpty;
    }

    public boolean getRequired() {
        return this.required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean getIsInternational() {
        return this.isInternational;
    }

    public void setIsInternational(boolean isInternational) {
        this.isInternational = isInternational;
    }

    public List<Field> getFields() {
        return this.fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }
}

class Field {

    private boolean required;
    private boolean disabled;
    private String name;
    private String value;

    public boolean getRequired() {
        return this.required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean getDisabled() {
        return this.disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

enum Action {

    MODIFY_DOCUMENTS_AND_FFP,
    CHANGE_SEAT;

    Action() {
    }
}

