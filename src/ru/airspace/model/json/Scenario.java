package ru.airspace.model.json;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrey Semenyuk on 2017.
 */
public class Scenario {

    private List<Element> elements = new ArrayList<>();
    private int attempts = 1;
    private int repeatEvery = 5;

    public Scenario() {
        elements.add(new Element());
    }

    public Scenario(List<Element> elements) {

        if (elements == null || elements.size() == 0) {
            this.elements.add(new Element());
        } else {
            this.elements.addAll(elements);
        }
    }

    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> criterias) {
        this.elements = criterias;
    }

    public Integer getAttempts() {
        return attempts;
    }

    public void setAttempts(Integer attempts) {

        if (attempts == null) {
            return;
        }

        this.attempts = attempts;
    }

    public int getRepeatEvery() {
        return repeatEvery;
    }

    public void setRepeatEvery(int repeatEvery) {
        this.repeatEvery = repeatEvery;
    }

    public void addElement(String criteria) {
        getElements().add(new Element(criteria));
    }

    public void addElement(String criteria, boolean nearRow, boolean pair) {
        getElements().add(new Element(criteria, nearRow, pair));
    }

    public static class Element {

        private String criteria = "У окна";
        private boolean nearRow = false;
        private boolean pair = false;

        Element() {}

        Element(String criteria) {
            this.criteria = criteria;
        }

        public Element(String criteria, boolean nearRow, boolean pair) {
            this.criteria = criteria;
            this.nearRow = nearRow;
            this.pair = pair;
        }

        public ObservableValue<String> nearRowProperty() {

            return new SimpleStringProperty(nearRow ? "Да" : "Нет");
        }

        public ObservableValue<String> pairProperty() {

            return new SimpleStringProperty(pair ? "Да" : "Нет");
        }

        public ObservableValue<String> criteriaProperty() {

            return new SimpleStringProperty(criteria);
        }

        public String getCriteria() {
            return criteria;
        }

        public void setCriteria(String criteria) {
            this.criteria = criteria;
        }

        public boolean isNearRow() {
            return nearRow;
        }

        public void setNearRow(boolean nearRow) {
            this.nearRow = nearRow;
        }

        public boolean isPair() {
            return pair;
        }

        public void setPair(boolean pair) {
            this.pair = pair;
        }

        @Override
        public boolean equals(Object object) {

            if (object instanceof Element) {
                Element element = (Element) object;
                return criteria.equals(element.getCriteria()) && nearRow == element.isNearRow() && pair == element.isPair();
            }

            return false;
        }
    }
}
