package ru.airspace.model.json;

import ru.airspace.model.Job;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrey Semenyuk on 2017.
 */
public class Aircraft {

    private List<Cabin> cabins = new ArrayList<>();

    public List<Cabin> getCabins() {
        return cabins;
    }

    public void setCabins(List<Cabin> cabins) {
        this.cabins = cabins;
    }

    public String toHtml() {

        StringBuilder html = new StringBuilder("<html><head>");
        html.append(getStyles());
        html.append(getJavaScript());
        html.append("</head><body>");

        for (Cabin cabin : getCabins()) {

            html.append("<table><tbody><tr class=\"label-row\">")
                    .append(getHeaderTable(cabin.getxCount()))
                    .append("</tr>");

            Place[][] places = cabin.getPlaces();

            for (int row = 0; row < cabin.getyCount(); row++) {
                html.append("<tr>");

                for (int col = 0; col < cabin.getxCount(); col++) {
                    if (places[col][row] == null) {
                        html.append("<td class=\"label-cell\">").append(String.valueOf(row + 1)).append("</td>");
                        continue;
                    }

                    switch (places[col][row].getStatus()) {
                        case "OCCUPIED":
                            html.append("<td class=\"seat-engaged\">");
                            break;
                        case "OTHER":
                            html.append("<td class=\"seat-other\">");
                            break;
                        case "SELECTED":
                            html.append("<td class=\"seat-selected\">");
                            break;
                        default:
                            html.append("<td>");
                    }

                    html.append(places[col][row].getSeatNumber()).append("</td>");
                }

                html.append("</tr>");
            }

            html.append("</tbody></table>");
        }

        html.append("</body></html>");

        return html.toString();
    }

    private String getStyles() {

        return new StringBuilder("<style type=\"text/css\">")
                .append("table tr td {border:1px solid;text-align:center}")
                .append(".label-row td {border:0}")
                .append(".label-cell {border:0}")
                .append(".seat-engaged {background:#cee2d3;}")
                .append(".seat-selected {background:#95C12A;}")
                .append(".seat-other {background:#dddddd;}")
                .append(".seat-confirmed {background:#2A95C1;}")
                .append("</style>")
                .toString();
    }

    private String getJavaScript() {

        return new StringBuilder("<script language='javascript'>")
                .append("document.addEventListener('DOMContentLoaded', function(){")
                .append("var cells=document.getElementsByTagName(\"td\");")
                .append("for (var i=0; i<cells.length; i++) {")
                .append("if (cells[i].classList.contains(\"label-cell\")) continue;")
                .append("cells[i].onmouseover = function() {this.bgColor = '#ff11ff'};")
                .append("cells[i].onmouseout = function() {this.bgColor = '#fff'};")
                .append("cells[i].onclick = function() {")
                .append("if (!javaMember.seatChange(this.innerText)) return;")
                .append("if (this.classList.contains(\"seat-selected\")) {")
                .append("this.classList.remove(\"seat-selected\");")
                .append("} else {")
                .append("this.classList.add(\"seat-selected\");")
                .append("}")
                .append("}")
                .append("}")
                .append("})")
                .append("</script>")
                .toString();
    }

    private String getHeaderTable(int xCount) {
        switch (xCount) {
            case 7:
                return "<td>A</td><td>B</td><td>C</td><td></td><td>D</td><td>E</td><td>F</td>";
            case 10:
                return "<td>A</td><td>B</td><td></td><td>C</td><td>D</td><td>E</td><td>F</td><td></td><td>G</td><td>H</td>";
            case 11:
                return "<td>A</td><td>B</td><td>C</td><td></td><td>D</td><td>E</td><td>F</td><td></td><td>G</td><td>H</td><td>K</td>";
            case 12:
                return "<td>A</td><td>B</td><td>C</td><td></td><td>D</td><td>E</td><td>F</td><td>G</td><td></td><td>H</td><td>K</td><td>L</td>";
        }
        return "";
    }

    public boolean findPlace(Job job) {

        for (Scenario.Element element : job.getScenario().getElements()) {
            getSelectedSeats().forEach(seat -> seat.setStatus("OTHER"));

            List<Place> foundPlaces;

            switch (element.getCriteria()) {
                case "У окна":
                    foundPlaces = findPlaceNearWidow(job.getTickets().size(), element.isNearRow());
                    break;
                case "У прохода":
                    foundPlaces = findPlaceNearAisle(job.getTickets().size(), element.isNearRow());
                    break;
                default:
                    foundPlaces = findPlaceAnyWhere(job.getTickets().size());
            }

            if (foundPlaces != null && foundPlaces.size() == job.getTickets().size()) {
                selectPlaces(foundPlaces);
                return true;
            }
        }

        return false;
    }

    public void selectPlaces(List<Place> places) {

        for (Cabin cabin : getCabins()) {
            Place place = cabin.getPlaces()[places.get(0).getX()][places.get(0).getY()];

            if (place == null) {
                continue;
            }

            places.forEach(foundPlace -> cabin.getPlaces()[foundPlace.getX()][foundPlace.getY()].setStatus("SELECTED"));
        }
    }

    private List<Place> findPlaceAnyWhere(int count) {

        List<Place> foundPlaces = new ArrayList<>();

        for (Cabin cabin : getCabins()) {
            for (int row = 0; row < cabin.getyCount(); row++) {
                for (int col = 1; col < cabin.getxCount() - 1; col++) {

                    if (cabin.getPlaces()[col][row] != null && cabin.getPlaces()[col][row].isFree()) {
                        foundPlaces.add(cabin.getPlaces()[col][row]);

                        if (foundPlaces.size() == count) {
                            return foundPlaces;
                        }
                    }
                }
            }
        }

        return foundPlaces;
    }

    private List<Place> findPlaceNearAisle(int count, boolean withNearRow) {

        List<Place> foundPlaces = new ArrayList<>();

        for (Cabin cabin : getCabins()) {

            for (int row = 0; row < cabin.getyCount(); row++) {

                List<Integer> rowIndexes = new ArrayList<>();

                for (int col = 0; col < cabin.getxCount(); col++) {
                    if (cabin.getPlaces()[col][row] == null) {
                        rowIndexes.add(col);
                    }
                }

                boolean freePlaceExist = false;

                for (Integer col : rowIndexes) {
                    if (cabin.getPlaces()[0][col - 1].isFree() || cabin.getPlaces()[0][col + 1].isFree()) {
                        freePlaceExist = true;
                        break;
                    }
                }

                if (!freePlaceExist) {
                    foundPlaces.clear();
                    continue;
                }

                int c;
                int inRowCount = foundPlaces.size() != 0 ? count - foundPlaces.size() : count;

                for (Integer col : rowIndexes) {
                    if (foundPlaces.size() != 0 && foundPlaces.get(0).getX() + 1 != col && foundPlaces.get(0).getX() - 1 != col) {
                        continue;
                    }

                    c = 0;
                    int x = col - 1;

                    while (c < inRowCount) {
                        if (x - c < 0 || cabin.getPlaces()[x - c][row] == null || !cabin.getPlaces()[x - c][row].isFree()) {
                            break;
                        }

                        c++;
                    }

                    if (c != inRowCount) {
                        if (withNearRow) {
                            if (foundPlaces.size() != 0 && !cabin.getPlaces()[col + 1][row].isFree()) {
                                foundPlaces.clear();
                            }

                            while (x >= 0 && cabin.getPlaces()[x][row] != null && cabin.getPlaces()[x][row].isFree()) {
                                foundPlaces.add(cabin.getPlaces()[x][row]);
                                x--;
                            }

                            if (!cabin.getPlaces()[col + 1][row].isFree()) {
                                continue;
                            }
                        }
                    } else {
                        for (int j = 0; j < inRowCount; j++) {
                            if (cabin.getPlaces()[x - j][row] != null) {
                                foundPlaces.add(cabin.getPlaces()[x - j][row]);
                            }
                        }

                        return foundPlaces;
                    }

                    c = 0;
                    x = col + 1;
                    inRowCount = foundPlaces.size() != 0 ? count - foundPlaces.size() : count;

                    while (c < inRowCount) {
                        if (x + c == cabin.getxCount() || cabin.getPlaces()[x + c][row] == null || !cabin.getPlaces()[x + c][row].isFree()) {
                            break;
                        }

                        c++;
                    }

                    if (c != inRowCount) {
                        if (withNearRow) {
                            if (foundPlaces.size() != 0) {
                                foundPlaces.clear();
                            }

                            while (x < cabin.getxCount() && cabin.getPlaces()[x][row] != null && cabin.getPlaces()[x][row].isFree()) {
                                foundPlaces.add(cabin.getPlaces()[x][row]);
                                x++;
                            }

                            continue;
                        }
                    } else {
                        for (int j = 0; j < inRowCount; j++) {
                            if (cabin.getPlaces()[x + j][row] != null) {
                                foundPlaces.add(cabin.getPlaces()[x + j][row]);
                            }
                        }

                        return foundPlaces;
                    }
                }

                if (foundPlaces.size() == count) {
                    return foundPlaces;
                }
            }
        }

        return null;
    }

    private List<Place> findPlaceNearWidow(int count, boolean withNearRow) {

        List<Place> foundPlaces = new ArrayList<>();

        for (Cabin cabin : getCabins()) {

            for (int row = 0; row < cabin.getyCount(); row++) {

                int cursor = 0;
                int inRowCount = foundPlaces.size() != 0 ? count - foundPlaces.size() : count;

                if (foundPlaces.size() == 0 ? cabin.getPlaces()[0][row].isFree() : foundPlaces.get(0).getX() == 0) {

                    if (foundPlaces.size() != 0 && !cabin.getPlaces()[0][row].isFree()) {
                        foundPlaces.clear();
                        continue;
                    }

                    while (cursor < inRowCount) {
                        if (cabin.getPlaces()[cursor][row] == null ? withNearRow : !cabin.getPlaces()[cursor][row].isFree()) {
                            break;
                        }

                        if (cabin.getPlaces()[cursor][row] == null) {
                            inRowCount++;
                        }

                        cursor++;
                    }

                    if (cursor != inRowCount) {
                        if (withNearRow) {
                            if (foundPlaces.size() != 0) {
                                foundPlaces.clear();
                            }

                            int j = 0;

                            while (cabin.getPlaces()[j][row] != null && cabin.getPlaces()[j][row].isFree()) {
                                foundPlaces.add(cabin.getPlaces()[j][row]);
                                j++;
                            }

                            continue;
                        }
                    } else {
                        for (int j = 0; j < inRowCount; j++) {
                            if (cabin.getPlaces()[j][row] != null) {
                                foundPlaces.add(cabin.getPlaces()[j][row]);
                            }
                        }

                        return foundPlaces;
                    }
                }

                cursor = 0;
                inRowCount = foundPlaces.size() != 0 ? count - foundPlaces.size() : count;
                int x = cabin.getxCount() - 1;

                if (foundPlaces.size() == 0 ? cabin.getPlaces()[x][row].isFree() : foundPlaces.get(0).getX() == x) {
                    if (foundPlaces.size() != 0 && !cabin.getPlaces()[x][row].isFree()) {
                        foundPlaces.clear();
                        continue;
                    }

                    while (cursor < inRowCount) {
                        if (cabin.getPlaces()[x - cursor][row] == null ? withNearRow : !cabin.getPlaces()[x - cursor][row].isFree()) {
                            break;
                        }

                        if (cabin.getPlaces()[x - cursor][row] == null) {
                            inRowCount++;
                        }

                        cursor++;
                    }

                    if (cursor != inRowCount) {
                        if (withNearRow) {
                            if (foundPlaces.size() != 0) {
                                foundPlaces.clear();
                            }

                            int j = x;

                            while (cabin.getPlaces()[j][row] != null && cabin.getPlaces()[j][row].isFree()) {
                                foundPlaces.add(cabin.getPlaces()[j][row]);
                                j--;
                            }

                            continue;
                        }
                    } else {
                        for (int j = 0; j < inRowCount; j++) {
                            if (cabin.getPlaces()[x - j][row] != null) {
                                foundPlaces.add(cabin.getPlaces()[x - j][row]);
                            }
                        }

                        return foundPlaces;
                    }
                }
            }

            if (foundPlaces.size() == count) {
                return foundPlaces;
            }
        }

        return null;
    }

    private List<Place> getSelectedSeats() {

        List<Place> result = new ArrayList<>();

        for (Cabin cabin : getCabins()) {

            for (int x = 0; x < cabin.getxCount(); x++) {
                for (int y = 0; y < cabin.getyCount(); y++) {
                    if (cabin.getPlaces()[x][y] != null && cabin.getPlaces()[x][y].getStatus().equals("SELECTED")) {
                        result.add(cabin.getPlaces()[x][y]);
                    }
                }
            }
        }

        return result;
    }

    public ArrayList<String> getSelectedSeatNumbers() {

        ArrayList<String> result = new ArrayList<>();

        List<Place> selectedSeats = getSelectedSeats();

        if (selectedSeats.size() > 0) {
            selectedSeats.forEach(seat -> result.add(seat.getSeatNumber()));
        }

        return result;
    }

    public void setOccupiedPlace(String badPlace) {

        for (Cabin cabin : getCabins()) {
            getSelectedSeats().forEach(seat -> cabin.getPlaces()[seat.getX()][seat.getY()].setStatus(seat.getSeatNumber().equals(badPlace) ? "OCCUPIED" : "FREE"));
        }
    }

    public String getSeatStatus(String seatNumber) {

        for (Cabin cabin : getCabins()) {
            for (int x = 0; x < cabin.getxCount(); x++) {
                for (int y = 0; y < cabin.getyCount(); y++) {
                    if (cabin.getPlaces()[x][y] != null && cabin.getPlaces()[x][y].getSeatNumber().equals(seatNumber)) {
                        return cabin.getPlaces()[x][y].getStatus();
                    }
                }
            }
        }

        return "";
    }

    public void setSeatStatus(String seatNumber, String seatStatus) {

        for (Cabin cabin : getCabins()) {
            for (int x = 0; x < cabin.getxCount(); x++) {
                for (int y = 0; y < cabin.getyCount(); y++) {
                    if (cabin.getPlaces()[x][y] != null && cabin.getPlaces()[x][y].getSeatNumber().equals(seatNumber)) {
                        cabin.getPlaces()[x][y].setStatus(seatStatus);
                        return;
                    }
                }
            }
        }
    }
}

class Cabin {

    private String num;
    private int xCount;
    private int yCount;
    private Place[][] places;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public int getxCount() {
        return xCount;
    }

    public void setxCount(int xCount) {
        this.xCount = xCount;
    }

    public int getyCount() {
        return yCount;
    }

    public void setyCount(int yCount) {
        this.yCount = yCount;
    }

    public Place[][] getPlaces() {
        return places;
    }

    public void setPlaces(List<Place> places) {
        this.places = new Place[xCount][yCount];
        places.forEach(place -> this.places[place.getX()][place.getY()] = place);
    }
}

class Place {

    private boolean partition;
    private int x;
    private int y;
    private String seatNumber;
    private String status;
    private String rate;

    public boolean isPartition() {
        return partition;
    }

    public void setPartition(boolean partition) {
        this.partition = partition;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber = seatNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    private boolean isPartition;

    public boolean isFree() {
        return status.equals("FREE");
    }
}