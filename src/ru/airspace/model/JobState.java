package ru.airspace.model;

/**
 * Перечисление возможных состояний задания
 * <p>
 * Created by Andrey Semenyuk on 2017.
 */
public enum JobState {

    ACTIVE("Активно", 3),
    ARCHIVED("Архивный", 99),
    EDITABLE("Редактируется", 0),
    ERROR("Ошибка", 1),
    CHECKED("Зарегистрирован", 4),
    CHECKIN("Регистрация", 5),
    COMPLETED("Завершено", 11),
    READY("Готов к регистрации", 2),
    NEW("Новое", 7);

    private String description;
    private int sortOrder;

    public int getSortOrder() {
        return sortOrder;
    }

    public String getDescription() {
        return description;
    }

    JobState(String description, int sortOrder) {
        this.description = description;
        this.sortOrder = sortOrder;
    }

    public boolean canCheckIn() {
        return (this == READY || this == CHECKED || this == COMPLETED);
    }

    public boolean isReverseSorting() {
        return sortOrder > 10;
    }
}
