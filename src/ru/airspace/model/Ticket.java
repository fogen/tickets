package ru.airspace.model;

import ru.airspace.handlers.DbHandler;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import ru.airspace.model.json.OrderInfo;

import java.security.SecureRandom;
import java.time.LocalDateTime;

/**
 * Created by Andrey Semenyuk on 2017.
 */
public class Ticket {

    private int id;
    private int jobId;

    private Job job;
    private String sessionId;
    private String selectedPlace;
    private OrderInfo orderInfo;

    private transient final StringProperty lastName;
    private transient final StringProperty number;
    private transient final StringProperty seatNumber = new SimpleStringProperty("");

    public Ticket(int id, String lastName, String number, int jobId, String seatNumber) {
        this(lastName, number, jobId);
        setSeatNumber(seatNumber);
        this.id = id;
    }

    public Ticket(String lastName, String number) {
        this.lastName = new SimpleStringProperty(lastName);
        this.number = new SimpleStringProperty(number);
    }

    private Ticket(String lastName, String number, int jobId) {
        this(lastName, number);
        this.jobId = jobId;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName.get();
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public String getNumber() {
        return number.get();
    }

    public StringProperty numberProperty() {
        return number;
    }

    public void setNumber(String number) {
        this.number.set(number);
    }

    public String getSeatNumber() {
        return seatNumber.get() == null ? "" : seatNumber.get();
    }

    public StringProperty seatNumberProperty() {
        return seatNumber;
    }

    public void setSeatNumber(String seatNumber) {
        this.seatNumber.set(seatNumber);
    }

    public void save() {
        DbHandler.getInstance().saveTicket(this);
    }

    public static Ticket get(int id) {
        return DbHandler.getInstance().getTicket(id);
    }

    public boolean validate() {
        return getLastName() != null && !getLastName().equals("") && getNumber() != null && !getNumber().equals("");
    }

    public void delete() {
        DbHandler.getInstance().deleteTicket(id);
    }

    public ObservableValue<Boolean> checkInProperty() {
        return new SimpleBooleanProperty(true);
    }

    public ObservableValue<String> stateProperty() {

        if (getSeatNumber() != null && !getSeatNumber().equals("") || getJob().getDepartureDate().isBefore(LocalDateTime.now())) {
            return new SimpleStringProperty("Завершён");
        }

        return new SimpleStringProperty("Ожидает");
    }

    public String getSelectedPlace() {
        return selectedPlace;
    }

    public void setSelectedPlace(String selectedPlace) {
        this.selectedPlace = selectedPlace;
    }

    public String getSessionId() {

        if (sessionId == null) {
            sessionId = generateSessionId();
        }

        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    private static String generateSessionId() {

        final char[] hexArray = "0123456789ABCDEF".toCharArray();
        SecureRandom sRandom = new SecureRandom();
        byte[] bytes = sRandom.generateSeed(16);
        char[] hexChars = new char[bytes.length * 2];

        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }

        return "JSESSIONID=" + new String(hexChars);
    }

    public OrderInfo getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(OrderInfo orderInfo) {
        this.orderInfo = orderInfo;
    }

    public void updateSeatNumber() {

        if (orderInfo != null && orderInfo.getSeatNumber() != null) {
            if (getSeatNumber() == null || !getSeatNumber().equals(orderInfo.getSeatNumber())) {
                setSeatNumber(orderInfo.getSeatNumber());
                save();
            }
        }
    }
}
