package ru.airspace;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import ru.airspace.controllers.MainController;
import ru.airspace.handlers.JobHandler;
import ru.airspace.handlers.PrefHandler;
import ru.airspace.handlers.TrayHandler;
import ru.airspace.utils.LogUtils;

import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Основной класс приложения
 * <p>
 * Created by Andrey Semenyuk on 2017.
 */
public class MainApp extends Application {

    private final static String lockFile = "QWlyU3BhY2UK";
    private final static Logger logger = LogUtils.getLogger();
    private final static PrefHandler prefs = PrefHandler.getInstance();
    private static TrayHandler trayHandler;
    private Stage stage;

    @Override
    public void start(final Stage primaryStage) throws Exception {

        if (!lockInstance()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("AirSpace");
            alert.setHeaderText(null);
            alert.setContentText("Программа уже запущена!");
            alert.showAndWait();
            System.exit(0);
        }

        Updater updater = Updater.getInstance();

        // проверяем обновления
        if (updater.checkUpdateNeed()) {
            updater.startUpdate();
        }

        this.stage = primaryStage;
        Platform.setImplicitExit(false);
        trayHandler = TrayHandler.getInstance(this);

        FXMLLoader mainLoader = new FXMLLoader();
        mainLoader.setLocation(getClass().getResource("/ru/airspace/view/Main.fxml"));
        BorderPane mainPane = mainLoader.load();

        MainController mainController = mainLoader.getController();
        mainController.setPrimaryStage(stage);
        mainController.setMainApp(this);

        stage.setScene(new Scene(mainPane));
        stage.setTitle("AirSpace");
        stage.setWidth(prefs.getAppWidth());
        stage.setHeight(prefs.getAppHeight());
        stage.widthProperty().addListener((observableValue, oldSceneWidth, newSceneWidth) -> prefs.saveAppWidth(newSceneWidth));
        stage.heightProperty().addListener((observableValue, oldSceneHeight, newSceneHeight) -> prefs.saveAppHeight(newSceneHeight));
        stage.setOnCloseRequest(event -> switchStage());
        stage.getIcons().add(new Image("ru/airspace/icon.png"));
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    public void switchStage() {

        if (stage.isShowing()) {
            stage.hide();

            if (trayHandler != null) {
                trayHandler.getOpenItem().setLabel("Показать");
            }
        } else {
            stage.show();
            stage.toFront();

            if (trayHandler != null) {
                trayHandler.getOpenItem().setLabel("Скрыть");
            }
        }
    }

    public void quit() {

        logger.info("Close application");
        prefs.saveAll();
        JobHandler.getInstance().stopScheduler();
        Platform.exit();

        if (trayHandler != null) {
            trayHandler.removeIcon();
        }
    }

    private static boolean lockInstance() {
        try {
            final File file = new File(System.getProperty("java.io.tmpdir") + File.separator + lockFile);
            final RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
            final FileLock fileLock = randomAccessFile.getChannel().tryLock();

            if (fileLock != null) {
                Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                    try {
                        fileLock.release();
                        randomAccessFile.close();
                        file.delete();
                    } catch (Exception e) {
                        logger.log(Level.SEVERE, "Unable to remove lock file: " + lockFile, e);
                    }
                }));
                return true;
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Unable to create and/or lock file:" + lockFile, e);
        }

        return false;
    }

}
