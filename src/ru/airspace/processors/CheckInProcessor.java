package ru.airspace.processors;

import com.alibaba.fastjson.JSON;
import javafx.scene.control.Alert;
import ru.airspace.handlers.PrefHandler;
import ru.airspace.handlers.TrayHandler;
import ru.airspace.model.Job;
import ru.airspace.model.JobState;
import ru.airspace.model.Ticket;
import ru.airspace.model.json.Aircraft;
import ru.airspace.model.json.OrderInfo;
import ru.airspace.utils.LogUtils;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Andrey Semenyuk on 2017.
 */
public class CheckInProcessor implements Runnable {

    private final static Logger logger = LogUtils.getLogger();
    private static TrayHandler trayHandler = TrayHandler.getInstance();
    private Aircraft aircraft;
    private Job job;
    private String baseUrl;

    public CheckInProcessor(Job job) {
        this.job = job;
        baseUrl = job.getAirline().getBaseUrl();
    }

    @Override
    public void run() {

        if (job.getState() == JobState.EDITABLE) {
            return;
        }

        logger.info("Running job: " + job.getName());

        if (PrefHandler.getInstance().isCheckinManual()) {
            job.setState(JobState.READY);

            if (trayHandler != null) {
                trayHandler.notify("Задание \"" + job.getName() + "\" готово к регистрации");
            }

            String curTime = DateTimeProcessor.getCurrentDateTime().toLocalTime().toString();
            sendToTelegram("<b>Старт:</b> " + job.getName() + ", <b>Время:</b> " + curTime);
            job.save();
        } else {
            job.setState(JobState.CHECKIN);
            sendToTelegram("Авторегистрация задания: " + job.getName());
            boolean hasErrors = false;

            for (int attempt = 1; attempt <= job.getScenario().getAttempts(); attempt++) {
                hasErrors = false;

                if (job.getScenario().getAttempts() > 1) {
                    sendToTelegram("Попытка " + attempt + " из " + job.getScenario().getAttempts());
                }

                if (!updateSelectPlaces()) {
                    hasErrors = true;
                    continue;
                }

                for (Ticket ticket : job.getTickets()) {
                    String error = doCheckIn(ticket);

                    if (error != null) {
                        logger.log(Level.SEVERE, "Auto checkin error: " + error);
                        hasErrors = true;
                        continue;
                    }
                }
            }

            if (!hasErrors) {
                job.getTickets().forEach(Ticket::updateSeatNumber);
                logger.info("Auto checkin complete");
                job.setState(JobState.CHECKED);
            }

            job.save();
            sendToTelegram(job.getState() == JobState.ERROR ? "<b>Ошибка авторегистрации:</b> " + job.getName() : "<b>Успешно</b>");
        }
    }

    private String getTelegramUrl(String message) {

        String token = PrefHandler.getInstance().getTelegramToken();
        String chatId = PrefHandler.getInstance().getTelegramChatId();

        if (token == null || chatId == null) {
            return null;
        }

        return "https://api.telegram.org/bot" + token + "/sendMessage?chat_id=" + chatId + "&parse_mode=HTML&text=" + message;
    }

    private void sendToTelegram(String message) {

        try {
            String url = getTelegramUrl(URLEncoder.encode(message));

            if (url == null) {
                return;
            }

            HttpsURLConnection connection = (HttpsURLConnection) new URL(url).openConnection();
            connection.addRequestProperty("User-Agent", "Tickets app");

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Failed to send message to telegram", ex);
        }
    }

    private void updateCookie(HttpURLConnection connection, Ticket ticket) {

        StringBuilder sb = new StringBuilder();
        List<String> cookies = connection.getHeaderFields().get("Set-Cookie");

        if (cookies == null || cookies.size() == 0) {
            return;
        }

        cookies.forEach(cookie -> {
            if (sb.length() > 0) {
                sb.append("; ");
            }
            String value = cookie.split(";")[0];
            sb.append(value);
        });

        logger.info("Old cookie: " + ticket.getSessionId() + " new cookie: " + sb.toString());

        ticket.setSessionId(sb.toString());
    }

    public void updateOrderInfo(Ticket ticket) {

        try {
            URL url = new URL(baseUrl + "json/order-info");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Cookie", ticket.getSessionId());

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                in.close();

                if (response.toString().contains("cancelSession")) {
                    logger.log(Level.SEVERE, "Failed to get order info. Response: " + response);
                    showErrorAlert("Ошибка регистрации");
                    return;
                }

                ticket.setOrderInfo(JSON.parseObject(response.toString(), OrderInfo.class));
                updateCookie(connection, ticket);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to get order info." + e.getMessage());
        }
    }

    public void getOrderInfo(Ticket ticket) {

        String params = String.format("lastName=%s&number=%s&date=%s&flight=%s", ticket.getLastName(), ticket.getNumber(), job.getDepartureDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")), job.getFlightNumber());

        try {
            URL url = new URL(baseUrl + "json/add-to-order?" + params);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                if (!response.toString().contains("\"result\":\"OK\"")) {
                    String errorMessage = "К сожалению, произошёл сбой обработки запроса. Попробуйте снова позже.";

                    if (response.toString().contains("orderNotFound")) {
                        errorMessage = "Билет не найден или регистрация ещё не началась.";
                    }

                    logger.log(Level.SEVERE, "Failed to add to order. Response: " + response);
                    showErrorAlert(errorMessage);
                    return;
                }
            }

            updateCookie(connection, ticket);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to add to order." + e.getMessage());
        }

        updateOrderInfo(ticket);

        try {
            URL url = new URL(baseUrl + "json/select-passengers?selected=" + URLEncoder.encode("{\"orderParts\":[{\"passengers\":[true]}]}", "UTF-8"));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Cookie", ticket.getSessionId());
            updateCookie(connection, ticket);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to select passengers." + e.getMessage());
        }

        updateOrderInfo(ticket);
    }

    public void downloadBoardingPass() {

        for (Ticket ticket : job.getTickets()) {

            getOrderInfo(ticket);

            try {
                URL url = new URL(baseUrl + "boarding-pass.pdf");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("Cookie", ticket.getSessionId());

                InputStream is = connection.getInputStream();
                String fileName = String.format("%s_%s_%s.pdf", job.getDepartureDate().toLocalDate().toString(), ticket.getLastName(), ticket.getNumber());

                File boardingPassDir = new File("boardingPass");

                if (!boardingPassDir.exists()) {
                    if (!boardingPassDir.mkdir()) {
                        logger.warning("Can't to create boardingPassDir");
                        return;
                    }
                }

                FileOutputStream fos = new FileOutputStream(boardingPassDir + "/" + fileName);

                byte[] buffer = new byte[4096];
                int bytesRead;

                while ((bytesRead = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }

                fos.close();
                is.close();
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Failed to get boarding pass." + e.getMessage());
            }
        }
    }

    private void showErrorAlert(String errorMessage) {

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Ошибка");
        alert.setHeaderText(null);
        alert.setContentText(errorMessage);
        alert.showAndWait();
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    private void loadAircraft() {

        if (job == null) {
            logger.log(Level.SEVERE, "Job don't set, can't to load aircraft");
            return;
        }

        Ticket ticket = job.getTickets().get(0);
        getOrderInfo(ticket);

        try {
            URLConnection urlConnection = new URL(baseUrl + "json/view-craft").openConnection();
            urlConnection.setRequestProperty("Cookie", ticket.getSessionId());
            urlConnection.connect();

            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            aircraft = JSON.parseObject(sb.toString(), Aircraft.class);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to load aircraft" + e.getMessage());
        }
    }

    public boolean updateSelectPlaces() {

        if (aircraft == null) {
            loadAircraft();

            if (aircraft == null) {
                return false;
            }
        }

        if (!aircraft.findPlace(job)) {
            return false;
        }

        ArrayList<String> selectedSeats = aircraft.getSelectedSeatNumbers();

        for (int i = 0; i < selectedSeats.size(); i++) {
            job.getTickets().get(i).setSelectedPlace(selectedSeats.get(i));
        }

        return true;
    }

    public void updateSelectPlaces(String badPlace) {

        aircraft.setOccupiedPlace(badPlace);
        updateSelectPlaces();
    }

    public boolean updateAircraftAndSelectPlaces() {

        loadAircraft();
        updateSelectPlaces();

        return aircraft != null;
    }

    public boolean checkinConfirm(Ticket ticket) {

        if (ticket.getOrderInfo() == null) {
            updateOrderInfo(ticket);
        }

        try {
            URL url = new URL(baseUrl + "json/confirm-check-in?" + URLEncoder.encode(ticket.getOrderInfo().getDocumentsAndFFP()));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Cookie", ticket.getSessionId());

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                in.close();

                if (!response.toString().contains("\"result\":\"ok\"")) {
                    return false;
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to confirm checkin" + e.getMessage());
        }

        return true;
    }

    public boolean selectSeatNumber(Ticket ticket) {

        try {
            String params = String.format("segmentIndex=%s&orderPartIndex=%s&passengerIndex=%s&seatNumber=%s", 0, 0, 0, ticket.getSelectedPlace());
            URL url = new URL(baseUrl + "json/select-seat-number?" + params);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Cookie", ticket.getSessionId());

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                in.close();

                if (!response.toString().contains("\"result\":\"ok\"")) {
                    return false;
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to select seat." + e.getMessage());
        }

        return true;
    }

    public boolean seatChange(String seatNumber) {

        String currentStatus = aircraft.getSeatStatus(seatNumber);

        if (currentStatus.equals("FREE")) {
            if (aircraft.getSelectedSeatNumbers().size() == job.getTickets().size()) {
                return false;
            }

        }

        aircraft.setSeatStatus(seatNumber, currentStatus.equals("FREE") ? "SELECTED" : "FREE");
        return true;
    }

    public boolean modifyDataSeat(Ticket ticket) {

        try {
            String params = String.format("segmentIndex=%s&orderPartIndex=%s&passengerIndex=%s&seatNumber=%s&passengerData=%s", 0, 0, 0, ticket.getSelectedPlace(), URLEncoder.encode(ticket.getOrderInfo().getPassengerData()));
            URL url = new URL(baseUrl + "json/modify-data-seat?" + params);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Cookie", ticket.getSessionId());

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }

                in.close();

                if (!response.toString().contains("\"result\":\"ok\"")) {
                    return false;
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to modify seat." + e.getMessage());
        }

        return true;
    }

    public String doCheckIn(Ticket ticket) {

        getOrderInfo(ticket);

        if (!ticket.getSeatNumber().equals("")) {
            if (!modifyDataSeat(ticket)) {
                return "Не удалось поменять место";
            }
        } else {
            if (!selectSeatNumber(ticket) || !checkinConfirm(ticket)) {
                return "Подтвердить места не удалось. Будет произведён новый поиск";
            }
        }

        updateOrderInfo(ticket);

        if (!ticket.getSeatNumber().equals("") && !ticket.getSelectedPlace().equals(ticket.getOrderInfo().getSeatNumber())) {
            return "Выбранное место не соответствует зарегистрированному. Будет произведён новый поиск";
        }

        return null;
    }
}
