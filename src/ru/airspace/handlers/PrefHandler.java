package ru.airspace.handlers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Обработчик настроек приложения
 * Created by Andrey Semenyuk on 2017.
 */
public class PrefHandler {

    private static PrefHandler instance = null;
    private static Properties appProperties = new Properties();

    /**
     * Получение экземляра обработчика
     *
     * @return PrefHandler
     */
    public static PrefHandler getInstance() {
        if (instance == null) {
            instance = new PrefHandler();
        }
        return instance;
    }

    private PrefHandler() {

        File f = new File("config.properties");

        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException ignored) {
            }
        }

        try {
            appProperties.load(new FileInputStream(f));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getTelegramToken() {
        return appProperties.getProperty("telegram.token", "312066192:AAHEbbCcYqkj463wVDIm0C8ou_sRMEPvbIc");
    }

    public String getTelegramChatId() {
        return appProperties.getProperty("telegram.chat", "-228942046");
    }

    public void saveAppWidth(Number newSceneWidth) {
        appProperties.setProperty("app.width", newSceneWidth.toString());
    }

    public void saveAppHeight(Number newSceneHeight) {
        appProperties.setProperty("app.height", newSceneHeight.toString());
    }

    public void saveAll() {
        try {
            appProperties.store(new FileOutputStream(new File("config.properties")), "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public double getAppWidth() {
        return Double.valueOf(appProperties.getProperty("app.width", "800"));
    }

    public double getAppHeight() {
        return Double.valueOf(appProperties.getProperty("app.height", "600"));
    }

    public String getUpdateUrl() {
        return appProperties.getProperty("update.url", "http://ekb.qilnet.ru:8080");
    }

    public boolean isJobShowHiddenOn() {
        return Boolean.valueOf(appProperties.getProperty("job.showHidden", "false"));
    }

    public void setJobShowHidden(Boolean value) {
        appProperties.setProperty("job.showHidden", value.toString());
    }

    public boolean isCheckinManual() {
        return Boolean.valueOf(appProperties.getProperty("checkin.manual", "true"));
    }

    public void setCheckinManual(Boolean value) {
        appProperties.setProperty("checkin.manual", value.toString());
    }

    public boolean isTrayNotifyEnabled() {
        return Boolean.valueOf(appProperties.getProperty("tray.notify", "true"));
    }

    public void setTrayNotifyEnabled(Boolean value) {
        appProperties.setProperty("tray.notify", value.toString());
    }
}
