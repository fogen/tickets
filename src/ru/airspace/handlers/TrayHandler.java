package ru.airspace.handlers;

import javafx.application.Platform;
import ru.airspace.MainApp;

import javax.swing.*;
import java.awt.*;
import java.net.URL;

/**
 * Обработчик работы с треем
 * Created by Andrey Semenyuk on 2017.
 */
public class TrayHandler {

    private static TrayHandler instance = null;
    private MenuItem openItem;
    private SystemTray tray;
    private TrayIcon trayIcon;
    private MainApp mainApp;

    /**
     * Получение экземляра обработчика
     *
     * @return TrayHandler
     */
    public static TrayHandler getInstance(MainApp mainApp) {

        if (instance == null) {
            instance = new TrayHandler();
            instance.mainApp = mainApp;
        }

        return instance;
    }

    public static TrayHandler getInstance() {
        return instance;
    }

    private TrayHandler() {

        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        SwingUtilities.invokeLater(this::createGui);
    }

    private void createGui() {

        try {
            Toolkit.getDefaultToolkit();
            tray = SystemTray.getSystemTray();

            openItem = new MenuItem("Скрыть");
            openItem.addActionListener(event -> Platform.runLater(mainApp::switchStage));
            Font defaultFont = Font.decode(null);
            Font boldFont = defaultFont.deriveFont(java.awt.Font.BOLD);
            openItem.setFont(boldFont);
            MenuItem exitItem = new MenuItem("Выход");
            exitItem.addActionListener(event -> mainApp.quit());
            final PopupMenu popup = new PopupMenu();
            popup.add(openItem);
            popup.addSeparator();
            popup.add(exitItem);
            URL imageUrl = this.getClass().getClassLoader().getResource("ru/airspace/icon.png");
            trayIcon = new TrayIcon(Toolkit.getDefaultToolkit().createImage(imageUrl), "Airspace", popup);
            trayIcon.addActionListener(event -> Platform.runLater(mainApp::switchStage));
            trayIcon.setImageAutoSize(true);
            tray.add(trayIcon);
        } catch (AWTException e) {
            e.printStackTrace();
        }
    }

    public MenuItem getOpenItem() {
        return openItem;
    }

    public void removeIcon() {
        tray.remove(trayIcon);
    }

    public void notify(String message) {
        if (PrefHandler.getInstance().isTrayNotifyEnabled()) {
            trayIcon.displayMessage("Уведомление", message, TrayIcon.MessageType.INFO);
        }
    }
}
