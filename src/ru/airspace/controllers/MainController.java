package ru.airspace.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.airspace.MainApp;
import ru.airspace.Updater;
import ru.airspace.handlers.JobHandler;
import ru.airspace.handlers.PrefHandler;
import ru.airspace.model.Job;
import ru.airspace.model.JobState;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by Andrey Semenyuk on 2017.
 */
public class MainController {

    private Stage primaryStage;

    @FXML
    private TableView<Job> jobsTable;
    @FXML
    private TableColumn<Job, String> nameCol;
    @FXML
    private TableColumn<Job, String> ticketsCol;
    @FXML
    private TableColumn<Job, LocalDateTime> departureCol;
    @FXML
    private TableColumn<Job, Number> timerToReg;
    @FXML
    private TableColumn<Job, JobState> stateCol;
    @FXML
    private CheckMenuItem showHiddenCheckMenu;

    private ObservableList<Job> jobs;
    private JobHandler jobHandler = JobHandler.getInstance();
    private final static PrefHandler prefs = PrefHandler.getInstance();
    private MainApp mainApp;

    public void setMainApp(MainApp mainApp) {
        this.mainApp = mainApp;
    }

    @FXML
    private void initialize() {

        jobs = jobHandler.getJobs();
        sortJobs();
        MenuItem item1 = new MenuItem("Изменить");
        item1.setOnAction(event -> handleUpdateJob());
        MenuItem item2 = new MenuItem("Скрыть");
        item2.setOnAction(event -> handleArchiveJobs());

        ContextMenu contextMenu = new ContextMenu();
        contextMenu.getItems().addAll(item1, item2);

        nameCol.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        ticketsCol.setCellValueFactory(cellData -> cellData.getValue().ticketsCount());
        ticketsCol.setStyle("-fx-alignment: CENTER;");

        DateTimeFormatter myDateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        departureCol.setCellFactory(column -> new TableCell<Job, LocalDateTime>() {
            @Override
            protected void updateItem(LocalDateTime item, boolean empty) {
                super.updateItem(item, empty);

                if (item != null && !empty) {
                    setText(myDateFormatter.format(item));
                }
            }
        });

        departureCol.setCellValueFactory(cellData -> cellData.getValue().departureDateProperty());
        departureCol.setStyle("-fx-alignment: CENTER;");

        timerToReg.setCellValueFactory(cellData -> cellData.getValue().secondsToRegProperty());
        timerToReg.setStyle("-fx-alignment: CENTER;");
        timerToReg.setCellFactory(tc -> new TableCell<Job, Number>() {
            @Override
            protected void updateItem(Number value, boolean empty) {
                super.updateItem(value, empty);

                if (value == null || value.intValue() == 0 || empty) {
                    setText("");
                } else {
                    int hours = value.intValue() / 3600;
                    int min = (value.intValue() - hours * 3600) / 60;
                    int sec = value.intValue() - min * 60 - hours * 3600;

                    if (hours >= 0 && min >= 0 && sec > 0) {
                        String text = hours > 0 && min > 0 ? String.format("%sч. %sм. %sс.", String.valueOf(hours), String.valueOf(min), String.valueOf(sec)) :
                                min > 0 ? String.format("%sм. %sс.", String.valueOf(min), String.valueOf(sec)) :
                                        String.format("%sс.", String.valueOf(sec));
                        setText(text);
                    } else {
                        setText("");
                    }
                }
            }
        });

        stateCol.setCellValueFactory(cellData -> cellData.getValue().stateProperty());
        stateCol.setCellFactory(column -> new TableCell<Job, JobState>() {
            @Override
            protected void updateItem(JobState item, boolean empty) {
                super.updateItem(item, empty);

                if (item != null && !empty) {
                    setText(item.getDescription());
                }
            }
        });
        stateCol.setStyle("-fx-alignment: CENTER;");

        jobsTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        jobsTable.setContextMenu(contextMenu);
        jobsTable.getContextMenu().setAutoHide(true);

        jobsTable.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() > 1) {
                handleUpdateJob(false);
            }
        });

        jobsTable.setItems(jobs);
        showHiddenCheckMenu.setSelected(prefs.isJobShowHiddenOn());
    }

    private void handleArchiveJobs() {

        ObservableList<Job> selectedJobs = FXCollections.observableArrayList(jobsTable.getSelectionModel().getSelectedItems());

        for (Job job : selectedJobs) {
            if (job.getState() == JobState.COMPLETED) {
                job.setState(JobState.ARCHIVED);
                jobs.remove(job);
                job.save();
            }
        }

        jobsTable.refresh();
    }

    @FXML
    private void handleAbout() {

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initOwner(primaryStage);
        alert.setTitle("О программе");
        alert.setHeaderText(null);
        ImageView imageView = new ImageView("ru/airspace/icon.png");
        imageView.setFitHeight(100);
        imageView.setFitWidth(100);
        alert.setGraphic(imageView);
        alert.setContentText("AirSpace\nВерсия: " + Updater.getInstance().getVersion());
        alert.showAndWait();
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private void showJobEditDialog(Job job) {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/JobEdit.fxml"));
            GridPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Редактирование задания");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setResizable(false);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            JobEditDialogController jobEditDialogController = loader.getController();
            jobEditDialogController.setDialogStage(dialogStage);
            jobEditDialogController.setJob(job);
            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showJobViewDialog(Job job) {

        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/JobView.fxml"));
            BorderPane page = loader.load();

            Stage dialogStage = new Stage();
            dialogStage.setTitle("Задание: " + job.getName());
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setResizable(false);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            JobViewDialogController jobViewDialogController = loader.getController();
            jobViewDialogController.setJob(job);
            dialogStage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleAddJob() {

        Job newJob = new Job();
        showJobEditDialog(newJob);

        if (newJob.getState() == JobState.NEW) {
            jobs.add(newJob);
            sortJobs();
        }
    }

    private void handleUpdateJob() {
        handleUpdateJob(true);
    }

    private void handleUpdateJob(boolean needEdit) {

        Job job = jobsTable.getSelectionModel().getSelectedItem();

        if (job != null) {
            if (job.getState().canCheckIn() && !needEdit) {
                showJobViewDialog(job);
                return;
            }

            showJobEditDialog(job);

            if (job.getState() == JobState.NEW) {
                jobHandler.updateJob(job);
                sortJobs();
                jobsTable.refresh();
            }
        }
    }

    public void handleQuit() {
        mainApp.quit();
    }

    public void handleShowHidden() {
        prefs.setJobShowHidden(showHiddenCheckMenu.isSelected());
    }

    private void sortJobs() {

        jobs.sort((job1, job2) -> {

            JobState state1 = job1.getState();
            JobState state2 = job2.getState();

            if (state1 == state2) {
                return state1.isReverseSorting() ? job2.getDepartureDate().compareTo(job1.getDepartureDate()) : job1.getDepartureDate().compareTo(job2.getDepartureDate());
            }

            return state1.getSortOrder() - state2.getSortOrder();
        });
    }
}