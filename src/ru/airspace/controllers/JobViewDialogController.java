package ru.airspace.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import netscape.javascript.JSObject;
import ru.airspace.model.Job;
import ru.airspace.model.JobState;
import ru.airspace.model.Ticket;
import ru.airspace.processors.CheckInProcessor;
import ru.airspace.utils.LogUtils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Logger;

/**
 * Created by Andrey Semenyuk on 2017.
 */
public class JobViewDialogController {

    @FXML
    private Label airlineLabel;
    @FXML
    private HBox criteriaBox;
    @FXML
    private Label flightNumberLabel;
    @FXML
    private Label departureDateLabel;
    @FXML
    private ChoiceBox criteriaChoiceBox;
    @FXML
    private CheckBox nearRowCheckBox;
    @FXML
    private CheckBox pairCheckBox;
    @FXML
    private Button checkInButton;
    @FXML
    private TableView<Ticket> ticketTable;
    @FXML
    private TableColumn<Ticket, String> lastNameCol;
    @FXML
    private TableColumn<Ticket, String> numberCol;
    @FXML
    private TableColumn<Ticket, String> seatCol;
    @FXML
    private TableColumn<Ticket, String> stateCol;

    private Job job;
    private ObservableList<Ticket> tickets = FXCollections.observableArrayList();
    private CheckInProcessor checkInProcessor;
    private final static Logger logger = LogUtils.getLogger();

    @FXML
    private void initialize() {
        ticketTable.setItems(tickets);

        lastNameCol.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
        lastNameCol.setStyle("-fx-alignment: CENTER;");

        numberCol.setCellValueFactory(cellData -> cellData.getValue().numberProperty());
        numberCol.setStyle("-fx-alignment: CENTER;");

        seatCol.setCellValueFactory(cellData -> cellData.getValue().seatNumberProperty());
        seatCol.setStyle("-fx-alignment: CENTER;");

        stateCol.setCellValueFactory(cellData -> cellData.getValue().stateProperty());
        stateCol.setStyle("-fx-alignment: CENTER;");

        ticketTable.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() > 1) {
                if (job.getState() == JobState.COMPLETED) {
                    checkInProcessor.downloadBoardingPass();
                }
            }
        });
    }

    public void setJob(Job job) {

        this.job = job;
        this.airlineLabel.setText(job.getAirline().getName());
        this.flightNumberLabel.setText(job.getFlightNumber());
        this.departureDateLabel.setText(job.getDepartureDate().format(DateTimeFormatter.ofPattern("dd.MM.YYYY HH:mm")));
        tickets.addAll(job.getTickets());

        checkInProcessor = new CheckInProcessor(job);

        if (job.getState().canCheckIn() || job.getDepartureDate().isAfter(LocalDateTime.now().plusDays(1))) {

            if (job.getState() != JobState.READY) {
                checkInButton.setText("Поменять места");
            }

            criteriaBox.setDisable(false);
            updateAdditionalCriteria();
        }
    }

    private void updateAdditionalCriteria() {

        if (tickets.size() > 1) {
            nearRowCheckBox.setDisable(false);
        }

        if (tickets.size() / 2 > 1 && tickets.size() % 2 == 0) {
            pairCheckBox.setDisable(false);
        }
    }

    public void handleCheckIn() {

        if (!checkInProcessor.updateAircraftAndSelectPlaces()) {
            return;
        }

        Button buttonRetry = new Button("Найти другой вариант");
        Button buttonCheckIn = new Button("Зарегистрировать");

        final WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();
        webEngine.setJavaScriptEnabled(true);
        webEngine.loadContent(checkInProcessor.getAircraft().toHtml());

        JSObject jsobj = (JSObject) webEngine.executeScript("window");
        jsobj.setMember("javaMember", new Bridge());

        buttonRetry.setOnAction(event -> {
            checkInProcessor.updateSelectPlaces();
            webEngine.loadContent(checkInProcessor.getAircraft().toHtml());
        });

        VBox root = new VBox(5);
        HBox buttonBox = new HBox(7);
        HBox buttonBox2 = new HBox(7);
        buttonBox2.getChildren().addAll(buttonRetry, buttonCheckIn);
        root.setPadding(new Insets(5));
        root.getChildren().addAll(buttonBox, buttonBox2, browser);

        Scene scene = new Scene(root);

        Stage stage = new Stage();
        stage.setTitle("Регистрация");
        stage.setScene(scene);
        stage.setWidth(600);
        stage.setHeight(600);

        buttonCheckIn.setOnAction(event -> {

            String error = null;

            for (Ticket ticket : tickets) {
                error = checkInProcessor.doCheckIn(ticket);

                if (error != null) {
                    reloadContentWithError(ticket, webEngine, error);
                    break;
                }
            }

            if (error == null) {
                tickets.forEach(Ticket::updateSeatNumber);
                ticketTable.refresh();
                job.setState(JobState.CHECKED);
                job.save();
                logger.info("Registration complete");
                stage.close();
            }
        });

        stage.show();
    }

    public class Bridge {

        public boolean seatChange(String seatNumber) {
            return checkInProcessor.seatChange(seatNumber);
        }
    }

    private void reloadContent(Ticket ticket, WebEngine webEngine) {
        checkInProcessor.updateSelectPlaces(ticket.getSelectedPlace());
        webEngine.loadContent(checkInProcessor.getAircraft().toHtml());
    }

    private void reloadContentWithError(Ticket ticket, WebEngine webEngine, String errorMessage) {

        if (!errorMessage.equals("")) {
            logger.warning(errorMessage);
        }

        reloadContent(ticket, webEngine);
    }
}
