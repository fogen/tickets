package ru.airspace.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import ru.airspace.handlers.DbHandler;
import ru.airspace.model.Airline;
import ru.airspace.model.Job;
import ru.airspace.model.JobState;
import ru.airspace.model.Ticket;
import ru.airspace.model.json.Scenario;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Andrey Semenyuk on 2017.
 */
public class JobEditDialogController {

    @FXML
    private TextField attemptField;
    @FXML
    private TabPane tabs;
    @FXML
    private ChoiceBox criteriaChoiceBox;
    @FXML
    private CheckBox nearRowCheckBox;
    @FXML
    private CheckBox pairCheckBox;
    @FXML
    private Button addElementButton;
    @FXML
    private TableView<Scenario.Element> scenarioTable;
    @FXML
    private TableColumn<Scenario.Element, String> criteriaCol;
    @FXML
    private TableColumn<Scenario.Element, String> nearRowCol;
    @FXML
    private TableColumn<Scenario.Element, String> pairCol;
    @FXML
    private HBox criteriaBox;
    @FXML
    private TextField descriptionField;
    @FXML
    private DatePicker departureDatePicker;
    @FXML
    private TextField departureTimeField;
    @FXML
    private TextField priorToRegField;
    @FXML
    private TextField flightNumberField;
    @FXML
    private ComboBox<Airline> airlineComboBox;
    @FXML
    private TextField lastNameField;
    @FXML
    private TextField ticketNumberField;
    @FXML
    private TableView<Ticket> ticketTable;
    @FXML
    private TableColumn<Ticket, String> lastNameCol;
    @FXML
    private TableColumn<Ticket, String> numberCol;
    @FXML
    private TableColumn<Ticket, String> placeCol;
    @FXML
    private TableColumn<Ticket, Boolean> checkInCol;

    private Stage dialogStage;
    private Job job;
    private JobState oldState;
    private ObservableList<Ticket> tickets = FXCollections.observableArrayList();
    private ObservableList<Airline> airlines = FXCollections.observableArrayList();
    private ObservableList<Scenario.Element> scenarioElements = FXCollections.observableArrayList();
    private List<Ticket> savedTickets;

    private static int MIN_PRIOR_TO_REG = 12;
    private static int MAX_PRIOR_TO_REG = 48;

    @FXML
    private void initialize() {

        ticketTable.setItems(tickets);
        scenarioTable.setItems(scenarioElements);
        airlines.setAll(DbHandler.getInstance().getAllAirlines());

        MenuItem item1 = new MenuItem("Удалить");
        item1.setOnAction(event -> handleDeleteTicket());
        ContextMenu contextMenu = new ContextMenu();
        contextMenu.getItems().addAll(item1);

        lastNameCol.setCellValueFactory(cellData -> cellData.getValue().lastNameProperty());
        lastNameCol.setStyle("-fx-alignment: CENTER;");

        numberCol.setCellValueFactory(cellData -> cellData.getValue().numberProperty());
        numberCol.setStyle("-fx-alignment: CENTER;");

        placeCol.setCellValueFactory(cellData -> cellData.getValue().seatNumberProperty());
        placeCol.setStyle("-fx-alignment: CENTER;");

        checkInCol.setCellValueFactory(cellData -> cellData.getValue().checkInProperty());
        checkInCol.setStyle("-fx-alignment: CENTER;");

        criteriaCol.setCellValueFactory(cellData -> cellData.getValue().criteriaProperty());
        criteriaCol.setStyle("-fx-alignment: CENTER;");
        nearRowCol.setCellValueFactory(cellData -> cellData.getValue().nearRowProperty());
        nearRowCol.setStyle("-fx-alignment: CENTER;");
        pairCol.setCellValueFactory(cellData -> cellData.getValue().pairProperty());
        pairCol.setStyle("-fx-alignment: CENTER;");

        descriptionField.textProperty().addListener((observableValue, oldValue, newValue) -> {
            if (newValue == null || newValue.length() == 0) {
                descriptionField.setStyle("-fx-border-color: red");
            } else {
                descriptionField.setStyle("");
            }
        });

        flightNumberField.textProperty().addListener((observableValue, oldValue, newValue) -> {
            if (newValue == null || newValue.length() == 0) {
                flightNumberField.setStyle("-fx-border-color: red");
            } else {
                flightNumberField.setStyle("");
            }
        });

        airlineComboBox.setItems(airlines);

        UnaryOperator<TextFormatter.Change> filterTimeField = change -> {
            if (!change.isContentChange()) {
                return change;
            }

            switch (change.getControlText().length()) {
                case 0:
                    if (!change.getControlNewText().matches("[0-2]") && change.getControlNewText().length() != 4) {
                        return null;
                    }
                    break;
                case 1:
                    if (!change.isDeleted() && !change.getControlNewText().matches("(0[0-9]|1[0-9]|2[0-3])")) {
                        return null;
                    }
                    break;
                case 2:
                    if (!change.isDeleted() && !change.getControlNewText().matches("(0[0-9]|1[0-9]|2[0-3])(:|[0-5])")) {
                        return null;
                    }
                    break;
                case 3:
                    if (!change.isDeleted() && !change.getControlNewText().matches("(0[0-9]|1[0-9]|2[0-3])(:[0-5]|[0-5][0-9])")) {
                        return null;
                    }
                    break;
                case 4:
                    if (!change.isDeleted() && !change.getControlNewText().matches("(0[0-9]|1[0-9]|2[0-3])(:[0-5][0-9])")) {
                        return null;
                    }
                    break;
            }

            return change;
        };

        StringConverter<String> converterTimeField = new StringConverter<String>() {
            @Override
            public String toString(String commitedText) {
                if (commitedText == null) {
                    return departureTimeField.getText();
                }

                if (commitedText.length() == 4 && commitedText.matches("(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])")) {
                    return String.format("%s:%s", commitedText.substring(0, 2), commitedText.substring(2, 4));
                }
                return departureTimeField.getText();
            }

            @Override
            public String fromString(String displayedText) {
                Pattern p = Pattern.compile("[\\p{Punct}\\p{Blank}]", Pattern.UNICODE_CHARACTER_CLASS);
                Matcher m = p.matcher(displayedText);
                displayedText = m.replaceAll("");

                if (displayedText.length() != 4) {
                    return null;
                }

                return displayedText;
            }
        };

        departureTimeField.setTextFormatter(new TextFormatter<>(converterTimeField, LocalTime.now().format(DateTimeFormatter.ofPattern("HHmm")), filterTimeField));

        priorToRegField.addEventFilter(KeyEvent.KEY_PRESSED, keyEvent -> {
            if (keyEvent.getCode() == KeyCode.DOWN) {
                handleDecrement();
                keyEvent.consume();
            }

            if (keyEvent.getCode() == KeyCode.UP) {
                handleIncrement();
                keyEvent.consume();
            }
        });

        priorToRegField.textProperty().addListener((observable, oldValue, newValue) -> {

            if (newValue.length() == 0 || !newValue.matches("\\d+")) {
                priorToRegField.setText("24");
                return;
            }

            if (oldValue.length() == 1 && Integer.valueOf(newValue) <= MIN_PRIOR_TO_REG) {
                priorToRegField.setText(String.valueOf(MIN_PRIOR_TO_REG));
                return;
            }

            if (Integer.valueOf(newValue) >= MAX_PRIOR_TO_REG) {
                priorToRegField.setText(String.valueOf(MAX_PRIOR_TO_REG));
            }
        });

        priorToRegField.focusedProperty().addListener((observable, oldValue, newValue) -> {

            if (!newValue) {
                if (Integer.valueOf(priorToRegField.getText()) <= MIN_PRIOR_TO_REG) {
                    priorToRegField.setText(String.valueOf(MIN_PRIOR_TO_REG));
                }
            }
        });

        ticketTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        ticketTable.setContextMenu(contextMenu);
        ticketTable.getContextMenu().setAutoHide(true);
    }

    private void handleDeleteTicket() {

        ObservableList<Ticket> selectedTickets = FXCollections.observableArrayList(ticketTable.getSelectionModel().getSelectedItems());

        for (Ticket ticket : selectedTickets) {
            ticket.delete();
        }

        tickets.removeAll(selectedTickets);
        ticketTable.refresh();
    }

    public void handleSave() {

        populateJobFields();

        boolean jobHasErrors = !job.validate();

        String errorMessage = null;

        if (jobHasErrors && tickets.size() == 0) {
            errorMessage = "Пожалуйста, заполните все поля и добавьте хотя бы один билет";
        } else {
            if (jobHasErrors) {
                errorMessage = "Пожалуйста, заполните все поля";
            }

            if (tickets.size() == 0) {
                errorMessage = "Пожалуйста, добавьте хотя бы один билет";
            }
        }

        if (errorMessage != null) {
            showErrorAlert(errorMessage);
            tabs.getSelectionModel().select(0);
            return;
        }

        job.setState(JobState.NEW);
        job.save();
        dialogStage.close();
    }

    private void populateJobFields() {

        if (descriptionField.getText().length() > 0) {
            job.setName(descriptionField.getText());
        }

        if (departureDatePicker.getValue() != null && departureTimeField.getText().length() == 5) {
            job.setDepartureDate(LocalDateTime.of(departureDatePicker.getValue(), LocalTime.parse(departureTimeField.getText())));
        }

        if (flightNumberField.getText() != null) {
            job.setFlightNumber(flightNumberField.getText());
        }

        if (tickets.size() == 0) {
            addTicket();
        }

        job.setTickets(tickets);
        job.setPriorToReg(Integer.valueOf(priorToRegField.getText()));
        job.setAirline(airlineComboBox.getSelectionModel().getSelectedItem());

        Scenario scenario = new Scenario(scenarioElements);

        if (!attemptField.getText().equals("1") && attemptField.getText().matches("[0-9]+")) {
            scenario.setAttempts(Integer.valueOf(attemptField.getText()));
        }

        job.setScenario(scenario);
    }

    public void handleCancel() {

        if (job.getId() > 0) {
            job.setState(oldState == JobState.ACTIVE ? JobState.NEW : oldState);
            job.getTickets().clear();
            job.getTickets().setAll(savedTickets);
        }

        dialogStage.close();
    }

    void setJob(Job job) {

        oldState = job.getState();

        job.setState(JobState.EDITABLE);

        this.job = job;
        descriptionField.setText(job.getName());
        flightNumberField.setText(job.getFlightNumber());

        if (job.getDepartureDate() != null) {
            departureDatePicker.setValue(job.getDepartureDate().toLocalDate());
            departureTimeField.setText(job.getDepartureDate().toLocalTime().format(DateTimeFormatter.ofPattern("HHmm")));
        }

        if (job.getId() > 0) {
            savedTickets = new ArrayList<>(job.getTickets());
            tickets = job.getTickets();
            ticketTable.setItems(tickets);
            scenarioElements = FXCollections.observableArrayList(job.getScenario().getElements());
            scenarioTable.setItems(scenarioElements);
            attemptField.setText(job.getScenario().getAttempts().toString());
        }

        if (job.getPriorToReg() != 24) {
            priorToRegField.setText(String.valueOf(job.getPriorToReg()));
        }

        if (job.getAirline() != null) {
            airlineComboBox.getSelectionModel().select(job.getAirline());
        }

        updateAdditionalCriteria();
    }

    void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.dialogStage.setOnCloseRequest(event -> handleCancel());
    }

    public void addTicket() {

        if (lastNameField.getText().length() > 0 && ticketNumberField.getText().length() > 0) {

            for (Ticket ticket : tickets) {
                if (ticket.getNumber().equals(ticketNumberField.getText())) {
                    ticketNumberField.setStyle("-fx-border-color: red");
                    return;
                }
            }

            ticketNumberField.setStyle("");
            lastNameField.setStyle("");

            Ticket ticket = new Ticket(lastNameField.getText(), ticketNumberField.getText());

            if (job.getId() > 0) {
                ticket.setJobId(job.getId());
                ticket.setJob(job);
            }

            tickets.add(ticket);
            lastNameField.clear();
            ticketNumberField.clear();
            ticketTable.refresh();

            updateAdditionalCriteria();
        } else {
            if (lastNameField.getText().length() == 0) {
                lastNameField.setStyle("-fx-border-color: red");
            }

            if (ticketNumberField.getText().length() == 0) {
                ticketNumberField.setStyle("-fx-border-color: red");
            }
        }
    }

    public void handleIncrement() {

        Integer value = Integer.valueOf(priorToRegField.getText());

        if (value >= MAX_PRIOR_TO_REG) {
            priorToRegField.setText(String.valueOf(MAX_PRIOR_TO_REG));
            return;
        }

        priorToRegField.setText((++value).toString());
    }

    public void handleDecrement() {

        Integer value = Integer.valueOf(priorToRegField.getText());

        if (value <= MIN_PRIOR_TO_REG) {
            priorToRegField.setText(String.valueOf(MIN_PRIOR_TO_REG));
            return;
        }

        priorToRegField.setText((--value).toString());
    }

    private void showErrorAlert(String errorMessage) {

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initOwner(dialogStage);
        alert.setTitle("Ошибка");
        alert.setHeaderText(null);
        alert.setContentText(errorMessage);
        alert.showAndWait();
    }

    private void updateAdditionalCriteria() {

        if (tickets.size() > 1) {
            nearRowCheckBox.setDisable(false);
        }

        if (tickets.size() / 2 > 1 && tickets.size() % 2 == 0) {
            pairCheckBox.setDisable(false);
        }
    }

    public void handleAddElement() {

        Scenario.Element newElement = new Scenario.Element(criteriaChoiceBox.getValue().toString(), nearRowCheckBox.isSelected(), pairCheckBox.isSelected());

        boolean exist = false;

        for (Scenario.Element element : scenarioElements) {
            if (element.equals(newElement)) {
                exist = true;
                break;
            }
        }

        if (!exist) {
            scenarioElements.add(newElement);
        }
    }
}
